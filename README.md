# big-hp
F5 Big-IP Honeypot

## Running the Honeypot

Begin by configuring the honeypot using the big-hp.env environment variable file. You can leave these variables blank
for defaults.

- IP_ADDRESS: The IP address of the system running the honeypot
- REPORTED_IP: The IP address to report in the honeypot logs
- REPORTED_PORT: The port number to report in the honeypot logs
- HOSTNAME: The hostname of the system running the honeypot

Next, build the container using docker-compose.

```
docker-compose build bighp
```

Start the container using docker-compose.

```
docker-compose up bighp
```

You should now be able to browse to https://<yourhostname> and see the honeypot page.

## Components of the Honeypot

### Output

The output directory contains the output plugins for the honeypot.

- ```__init__.py``` is the base logging class for the honeypot. This must be edited anytime a new plugin is added to
support the new output plugin.
- ```jsonlog.py``` is the JSON file output plugin for writing honeypot logs directly to disk.
- ```hpfeeds.py``` is an output plugin used by the Community Honey Network (CHN) platform.
- ```stingar.py``` is an output plugin used by the STINGARv2 platform.

### static

This directory contains the static content served from the web server.
- ```css```: static CSS files
- ```img```: static image files
- ```js```: static Javascript files

### templates

This directory contains the Jinja2 template files. These are mostly HTML files. This provides the ability
to generate dynamic HTML depending on honeypot configuration or user interaction.

### Main Python scripts

- ```app.py```: Contains the main web app paths and code to handle serving content.
- ```config.py```: Handles parsing the configuration file for the honeypot.
- ```utils.py```: Random utilities for the application. For this honeypot, mostly contains header configurations.

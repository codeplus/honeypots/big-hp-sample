#!/bin/bash

trap "exit 130" SIGINT
trap "exit 137" SIGKILL
trap "exit 143" SIGTERM

set -o errexit
set -o nounset
set -o pipefail


main () {

    DEBUG=${DEBUG:-false}
    if [[ ${DEBUG} == "true" ]]
    then
      set -o xtrace
      BIGHP_DEBUG="-v"
    else
      BIGHP_DEBUG=""
    fi

    # Keep old var names, but create also create some new ones that
    # containedenv can understand

    export BIGHP_bighp__ip_address="${IP_ADDRESS}"
    export BIGHP_bighp__reported_ip="${REPORTED_IP}"
    export BIGHP_bighp__reported_port="${REPORTED_PORT}"
    export BIGHP_bighp__hostname="${HOSTNAME}"

    # Write out custom conpot config
    containedenv-config-writer.py \
      -p BIGHP_ \
      -f ini \
      -r /opt/big-hp/conf/big-hp.cfg.template \
      -o /opt/big-hp/big-hp.cfg

    cd /opt/big-hp/big-hp
    gunicorn --certfile /opt/big-hp/cert.pem --keyfile /opt/big-hp/key.pem --access-logfile - -b 0.0.0.0:8000 -k gevent app
}

main "$@"

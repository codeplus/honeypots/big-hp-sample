FROM python:3.11-rc-alpine

ENV CONFIG "/opt/big-hp/big-hp.cfg"
ENV BIGHP_JSON "/etc/big-hp/big-hp.json"

# hadolint ignore=DL3018
RUN apk add --no-cache git python3-dev libffi-dev gcc g++ musl-dev make openssl bash jq

WORKDIR /opt
COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt && mkdir /opt/big-hp && mkdir /var/log/bighp
WORKDIR /opt/big-hp

COPY big-hp /opt/big-hp/big-hp
COPY conf /opt/big-hp/conf
COPY entrypoint.sh /opt/big-hp/entrypoint.sh
RUN chmod 0755 /opt/big-hp/entrypoint.sh

RUN openssl req -subj '/CN=localhost' -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365

ENTRYPOINT ["/opt/big-hp/entrypoint.sh"]
